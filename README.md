# Resto
Partie "salle" du projet C#/.NET (en go, dans linux, sans .NET).  
Implémente la vue et la partie du contrôleur gérant la salle.

### Installation
Installer [go](https://golang.org) et les dépendances de [pixel](https://github.com/faiface/pixel) et [ui](https://github.com/andlabs/ui), 
puis définir [`$GOPATH`](https://github.com/golang/go/wiki/SettingGOPATH) et ajouter `$GOPATH/bin` à `$PATH`
```bash
go get github.com/faiface/pixel
go get github.com/andlabs/ui/...
go get gitlab.com/pyjama/resto
resto
```
Documentation: `godoc -http :6060` puis ouvrir un navigateur à http://localhost:6060/pkg/gitlab.com/pyjama/resto
