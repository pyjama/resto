package main

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"
)

// Serv lance un simple serveur http remplaçant la cuisine en attendant de lier les deux parties de l'application
func Serv(port string, accel int) {
	temps := 0
	go incTime(&temps, accel)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		handle(w, r, temps)
	})
	http.ListenAndServe(":"+port, nil)
}

func handle(w http.ResponseWriter, r *http.Request, temps int) {
	w.Header().Set("Content-Type", "application/json")
	var req map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.Write([]byte("{\"type\": \"reponse\", \"error\": \"Impossible de parser le JSON en entrée\"}"))
		return
	}
	rep := make(map[string]interface{})
	switch req["type"] {
	case "bonjour":
		repBytes, err := ioutil.ReadFile("ressources/serveur/bonjour.json")
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(repBytes, &rep)
	case "commande", "materiel":
		time.Sleep(time.Duration(rand.Intn(4000)) * time.Millisecond)
	}
	rep["temps"] = temps
	repBytes, err := json.Marshal(rep)
	w.Write(repBytes)
}

// Incrémentation du temps
func incTime(t *int, accel int) {
	tick := time.Tick(time.Second / time.Duration(accel))
	for {
		<-tick
		*t++
	}
}
