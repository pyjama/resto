package controller

import (
	"fmt"
	"gitlab.com/pyjama/resto/view"
	"math/rand"
)

// Acteur est un élément effectuant une action à chaque seconde.
// Act est la fonction appellée à chaque seconde écoulée dans le resto.
type Acteur interface {
	Act()
}

// Client représente un groupe de clients
type Client struct {
	Resto    *Resto
	Table    *Table
	Sprite   *view.Sprite
	Etat     string
	Taille   int
	Entrées  []string
	Plats    []string
	Desserts []string
	Restant  int
}

func NewClient(r *Resto) *Client {
	c := Client{
		Resto:   r,
		Sprite:  r.Fenêtre.NewRandomSprite("emoji.png", 32, 32, 1.5),
		Etat:    "Se demande si le restaurant est ouvert",
		Taille:  int(rand.Float64()*rand.Float64()*5+1) * 2,
		Restant: 150,
	}
	c.Sprite.Pos(20, 350)
	c.Sprite.Nom = "Clients"
	c.Sprite.Desc = c.String
	r.Acteurs = append(r.Acteurs, &c)
	return &c
}

func (c *Client) String() string {
	str := fmt.Sprintf("%v\nGroupe de %v personnes\n\n%s\n\n", c.Restant, c.Taille, c.Etat)
	if c.Entrées != nil {
		str += "Commande:\n\nEntrées:\n"
		for _, v := range c.Entrées {
			str += v + ", "
		}
		str = str[:len(str)-2] + "\nPlats:\n"
		for _, v := range c.Plats {
			str += v + ", "
		}
		str = str[:len(str)-2] + "\nDesserts:\n"
		for _, v := range c.Desserts {
			str += v + ", "
		}
		str = str[:len(str)-2]
	}
	return str
}

func (c *Client) Act() {
	if c.Restant > 0 {
		c.Restant--
	}
	switch c.Etat {
	case "S'en va":
		if c.Sprite.Matrix[5] > 352 {
			c.Sprite.Move(0, -2)
		}
		if c.Sprite.Matrix[5] < 400 {
			c.Sprite.Move(-2, 0)
		}
	case "Se dirige vers le maître d'hôtel":
		if c.Sprite.Moveto(c.Resto.MaitreHotel.Sprite, 45, -25) {
			c.Resto.MaitreHotel.Queue = append(c.Resto.MaitreHotel.Queue, c)
			c.Etat = "Attend pour se faire attribuer une table"
		}
	case "Se dirige vers une table":
		if c.Sprite.Moveto(c.Table.Sprite, 0, 50) {
			c.Etat = "Choisit un plat"
			c.Restant = 250
		}
	case "Va payer":
		if c.Sprite.Moveto(c.Resto.MaitreHotel.Sprite, 45, 25) {
			c.Etat = "Paie"
			c.Restant = 120
		}
	}
	if c.Restant == 0 {
		switch c.Etat {
		case "Se demande si le restaurant est ouvert":
			if c.Resto.EstOuvert() {
				c.Etat = "Se dirige vers le maître d'hôtel"
				c.Restant = -1
			} else {
				c.Etat = "S'en va"
				c.Restant = 1000
			}
		case "Choisit un plat":
			if serveur := c.Table.Carré.ServeurLibre(); serveur != nil {
				c.Restant = -1
				c.Etat = "Attend qu'un serveur vienne prendre la commande"
				for i := 0; i < c.Taille; i++ {
					c.Entrées = append(c.Entrées, c.Resto.Entrées[rand.Intn(len(c.Resto.Entrées))])
					c.Plats = append(c.Plats, c.Resto.Plats[rand.Intn(len(c.Resto.Plats))])
					c.Desserts = append(c.Desserts, c.Resto.Desserts[rand.Intn(len(c.Resto.Desserts))])
				}
				serveur.Client = c
				serveur.Etat = "Se dirige vers un client pour prendre sa commande"
			}
		case "Donne la commande au serveur":
			c.Restant = -1
			c.Etat = "Attend son entrée"
		case "Mange son entrée", "Mange son plat", "Mange son dessert":
			if serveur := c.Table.Carré.ServeurLibre(); serveur != nil {
				serveur.Client = c
				serveur.Etat = "Se dirige vers un client pour le débarasser"
				c.ChangePlat()
			}
		case "Paie":
			c.Restant = -1
			c.Etat = "S'en va"
		}
	}
}

// ChangePlat fait changer l'étape du repas du client
// Il veut d'abord son entrée, puis son plat, puis son dessert.
func (c *Client) ChangePlat() {
	switch c.Etat {
	case "Mange son entrée":
		c.Etat = "Attend son plat"
		c.Table.Afficher("sale", 0.08)
	case "Mange son plat":
		c.Etat = "Attend son dessert"
		c.Table.Afficher("sale", 0.08)
	case "Mange son dessert":
		c.Etat = "Va payer"
		c.Table.Afficher("sale", 0.08)
		c.Table.Occupée = false
	case "Attend son entrée":
		c.Etat = "Mange son entrée"
		c.Table.Afficher("entrée", 0.08)
	case "Attend son plat":
		c.Etat = "Mange son plat"
		c.Table.Afficher("plat", 0.12)
	case "Attend son dessert":
		c.Etat = "Mange son dessert"
		c.Table.Afficher("dessert", 0.19)
	}
	c.Restant = -1
	if c.Etat[:4] == "Mang" {
		c.Restant = rand.Intn(2000)
	}
}
