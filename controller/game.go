package controller

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

// Game contient les restos et gère la liaison avec le serveur.
type Game struct {
	CmdId   int
	Adresse string
	Restos  []*Resto
}

// NewGame initialise les restaurants et établit la connection au serveur.
func NewGame(width, height int, adrese string) *Game {
	game := Game{
		CmdId:   0,
		Adresse: adrese,
	}

	// Première requête au serveur
	bonjour := make(map[string]interface{})
	bonjour["type"] = "bonjour"
	initMap, err := game.Req(bonjour)
	if err != nil {
		panic(err)
	}
	// Initialisation des restaurants
	for _, tmp := range initMap["restos"].([]interface{}) {
		resto := tmp.(map[string]interface{})
		horaires := resto["horaires"].([]interface{})
		h := make([][2]float64, len(horaires))
		for i, v := range horaires {
			t := v.([]interface{})
			for j, val := range t {
				h[i][j] = val.(float64)
			}
		}
		entrees := resto["entrees"].([]interface{})
		plats := resto["plats"].([]interface{})
		desserts := resto["desserts"].([]interface{})
		e := make([]string, len(entrees))
		p := make([]string, len(plats))
		d := make([]string, len(desserts))
		InterfaceArrayToStrArray(entrees, e)
		InterfaceArrayToStrArray(plats, p)
		InterfaceArrayToStrArray(desserts, d)
		game.Restos = append(game.Restos, NewResto(
			&game, width, height, int(resto["temps"].(float64)), int(resto["acceleration"].(float64)),
			h, e, p, d, resto["carres"].([]interface{}),
		))
		// Pixel semble être un peu moins cassé quand il n'a pas à créer plusieurs fenêtres à la fois
		time.Sleep(10 * time.Millisecond)
	}
	return &game
}

// CommandeId retourne un ID unique afin d'identifier les commandes
func (c *Game) CommandeId() int {
	c.CmdId++
	return c.CmdId
}

func InterfaceArrayToStrArray(interfaceArray []interface{}, strArray []string) {
	for i, v := range interfaceArray {
		switch v.(type) {
		case string:
			strArray[i] = v.(string)
		default:
			strArray[i] = ""
		}
	}
}

// Req effectue une requête au serveur et retourne une map du JSON retourné par le serveur
func (c *Game) Req(obj map[string]interface{}) (map[string]interface{}, error) {
	msg, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", c.Adresse, bytes.NewBuffer(msg))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var respMap map[string]interface{}
	if err = json.NewDecoder(resp.Body).Decode(&respMap); err != nil {
		return nil, err
	}
	return respMap, nil
}
