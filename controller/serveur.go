package controller

import (
	"fmt"
	"gitlab.com/pyjama/resto/view"
	"math/rand"
)

// Serveur prend et livre les commandes et guide les clients vers leur tables.
type Serveur struct {
	Carré  *Carré
	Etat   string
	Sprite *view.Sprite
	Client *Client // Le client dont le serveur est en train de s'occuper
}

func NewServeur(c *Carré) *Serveur {
	s := Serveur{
		Carré:  c,
		Sprite: c.Resto.Fenêtre.NewSprite("serveur.png", 0.3),
		Etat:   "Libre",
	}
	s.Sprite.Pos(rand.Float64()*1000, rand.Float64()*700)
	s.Sprite.Nom = "Un serveur"
	s.Sprite.Desc = s.String
	c.Resto.Acteurs = append(c.Resto.Acteurs, &s)
	return &s
}

func (s *Serveur) String() string {
	return s.Etat
}

// Act est l'action effectuée par le serveur à chaque type,
// elle dépend de l'état du serveur.
func (s *Serveur) Act() {
	switch s.Etat {
	case "Se dirige vers un groupe de clients pour le placer":
		if s.Sprite.Moveto(s.Client.Sprite, 30, 0) {
			s.Client.Etat = "Se dirige vers une table"
			s.Etat = "Se dirige vers une table pour placer les clients"
		}
	case "Se dirige vers une table pour placer les clients":
		if s.Sprite.Moveto(s.Client.Table.Sprite, 40, 0) {
			s.Etat = "Libre"
		}
	case "Se dirige vers un client pour prendre sa commande":
		if s.Sprite.Moveto(s.Client.Table.Sprite, 40, 0) {
			s.Etat = "Prend la commande du client"
			s.Client.Etat = "Donne la commande au serveur"
			s.Client.Restant = 60
		}
	case "Prend la commande du client":
		if s.Client.Etat == "Attend son entrée" {
			s.Etat = "Va donner la commande du client en cuisine"
		}
	case "Va donner la commande du client en cuisine":
		if s.Sprite.Moveto(s.Carré.Resto.MaitreHotel.Sprite, 1000, 100) {
			req := make(map[string]interface{})
			req["type"] = "commande"
			req["id"] = s.Carré.Resto.Game.CommandeId()
			cmd := make(map[string][]string)
			for i := range s.Client.Entrées {
				cmd["entrees"] = append(cmd["entrees"], s.Client.Entrées[i])
				cmd["plats"] = append(cmd["plats"], s.Client.Entrées[i])
				cmd["desserts"] = append(cmd["desserts"], s.Client.Entrées[i])
			}
			req["commande"] = cmd
			go func(client *Client) {
				s.Carré.Resto.Game.Req(req)
				s.Carré.Resto.MaitreHotel.QueuePlat = append(
					s.Carré.Resto.MaitreHotel.QueuePlat, client)
			}(s.Client)
			s.Etat = "Libre"
		}
	case "Se dirige vers la cuisine pour chercher une commande":
		if s.Sprite.Moveto(s.Carré.Resto.MaitreHotel.Sprite, 1000, 100) {
			s.Etat = "Se dirige vers un client pour lui apporter sa commande"
		}
	case "Se dirige vers un client pour lui apporter sa commande":
		if s.Sprite.Moveto(s.Client.Table.Sprite, 40, 0) {
			s.Etat = "Libre"
			s.Client.ChangePlat()
		}
	case "Se dirige vers un client pour le débarasser":
		if s.Sprite.Moveto(s.Client.Table.Sprite, 40, 0) {
			s.Etat = "Se dirige vers la cuisine pour ramener du matériel"
			s.Client.Table.Afficher("rien", 0.1)
		}
	case "Se dirige vers la cuisine pour ramener du matériel":
		if s.Sprite.Moveto(s.Carré.Resto.MaitreHotel.Sprite, 1000, 100) {
			req := make(map[string]interface{})
			req["type"] = "materiel"
			go func(client *Client) {
				s.Carré.Resto.Game.Req(req)
				//if s.Client.Etat != "Va payer" && s.Client.Etat != "S'en va" {
				s.Carré.Resto.MaitreHotel.QueuePlat = append(
					s.Carré.Resto.MaitreHotel.QueuePlat, client)
				//}
			}(s.Client)
			s.Etat = "Libre"
		}
	}
}

type MaitreHotel struct {
	Resto          *Resto
	Etat           string
	Sprite         *view.Sprite
	Queue          []*Client
	QueuePlat      []*Client
	ProchainClient int
}

func NewMaitreHotel(r *Resto) *MaitreHotel {
	maitre := MaitreHotel{
		Resto:          r,
		Sprite:         r.Fenêtre.NewSprite("maitrehotel.png", 1),
		ProchainClient: rand.Intn(300) + 1,
	}
	maitre.Sprite.Pos(40, 550)
	maitre.Sprite.Desc = maitre.String
	r.Acteurs = append(r.Acteurs, &maitre)
	return &maitre
}

func (m *MaitreHotel) String() string {
	return fmt.Sprintf(
		"Temps avant l'arrivée du prochain client:\n%v secondes", m.ProchainClient,
	)
}

// Le maître d'hotel fait apparaitre les clients et leur attribue une table
func (m *MaitreHotel) Act() {
	// Apparition des clients
	m.ProchainClient--
	if m.ProchainClient == 0 {
		NewClient(m.Resto)
		m.ProchainClient = rand.Intn(300) + 1
	}
	// Attribution d'une table à un client de la file, appel d'un serveur pour le placer
	for i, client := range m.Queue {
		if client.Table == nil {
			m.AttribueTable(client)
		}
		if client.Table != nil {
			if serveur := client.Table.Carré.ServeurLibre(); serveur != nil {
				serveur.Etat = "Se dirige vers un groupe de clients pour le placer"
				serveur.Client = client
				if i < len(m.Queue) {
					m.Queue = append(m.Queue[:i], m.Queue[i+1:]...)
				}
			}
		} else {
			if i == 3 {
				m.Queue = m.Queue[:i]
				client.Etat = "S'en va"
			}
		}
	}
	for i, client := range m.QueuePlat {
		if serveur := client.Table.Carré.ServeurLibre(); serveur != nil {
			serveur.Etat = "Se dirige vers la cuisine pour chercher une commande"
			serveur.Client = client
			//fmt.Println("i: ", i, "len: ", len(m.QueuePlat))
			if i < len(m.QueuePlat) {
				m.QueuePlat = append(m.QueuePlat[:i], m.QueuePlat[i+1:]...)
			}
		}
	}
}

// AtribueTable attribue la table libre la plus petite pour un groupe de clients.
// Ne fait rien si aucune table libre n'est disponible.
func (m *MaitreHotel) AttribueTable(client *Client) {
	for taille := client.Taille; taille <= 10; taille++ {
		for _, carré := range m.Resto.Carrés {
			for i := range carré.Tables {
				if carré.Tables[i].Taille == taille && !carré.Tables[i].Occupée {
					client.Table = carré.Tables[i]
					carré.Tables[i].Occupée = true
					return
				}
			}
		}
	}
}
