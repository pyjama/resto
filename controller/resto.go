package controller

import (
	"fmt"
	"gitlab.com/pyjama/resto/view"
	"math"
	"math/rand"
	"strconv"
	"time"
)

// Resto représente un restaurant
type Resto struct {
	Game        *Game
	Fenêtre     *view.Fenêtre
	Horaires    [][2]float64
	Entrées     []string
	Plats       []string
	Desserts    []string
	Carrés      []*Carré
	MaitreHotel *MaitreHotel
	Acteurs     []Acteur
	Temps       int
	accel       float64
	tick        <-chan time.Time
}

// NewResto crée un restaurant, place ses tables et ses serveurs
func NewResto(game *Game, width, height, temps, accel int,
	h [][2]float64, e, p, d []string, carrés []interface{}) *Resto {
	resto := Resto{
		Game:     game,
		Fenêtre:  view.NewFenêtre(width, height),
		Horaires: h,
		Entrées:  e,
		Plats:    p,
		Desserts: d,
		Temps:    temps,
		accel:    float64(accel),
	}
	for i := range h {
		for j := range h[i] {
			h[i][j] *= 3600
		}
	}
	// Placement et remplissage des carrés les carrés
	posCarrés := Répartit(width, height, len(carrés))
	for i := range posCarrés {
		// Empêche les tables d'apparaître dans les murs
		if posCarrés[i][0] == 0 {
			posCarrés[i][0] += 80
		}
		if posCarrés[i][1] == 0 {
			posCarrés[i][1] += 30
		}
		if posCarrés[i][2] == width {
			posCarrés[i][2] -= 30
		}
		if posCarrés[i][3] == height {
			posCarrés[i][3] -= 40
		}
		resto.Carrés = append(resto.Carrés, NewCarré(
			posCarrés[i], carrés[i].(map[string]interface{}), &resto))
	}
	resto.tick = time.Tick(time.Second / time.Duration(accel))
	resto.MaitreHotel = NewMaitreHotel(&resto)
	go resto.Loop()
	return &resto
}

// Loop est la "boucle principale".
// À chaque seconde écoulée dans le restaurant,
// elle exécute la méthode "Act" de tous les acteurs.
func (r *Resto) Loop() {
	for {
		select {
		case scroll := <-r.Fenêtre.Scroll:
			acc := r.accel + scroll/10*r.accel
			if acc > 1.2 {
				r.accel = acc
				r.tick = time.Tick(time.Second / time.Duration(r.accel))
			} else {
				if scroll > 0 {
					r.accel = 1.1
					r.tick = time.Tick(time.Second / time.Duration(1))
				} else {
					r.tick = nil
				}
			}
		case <-r.tick:
			r.Temps++
			if r.Temps == 86400 {
				r.Temps = 0
			}
			m := strconv.Itoa(r.Temps % 3600 / 60)
			if len(m) == 1 {
				m = "0" + m
			}
			h := strconv.Itoa(r.Temps / 3600)
			r.Fenêtre.Title = fmt.Sprintf("La salle du resto, %vh%v, %vfps", h, m, r.Fenêtre.Fps)
			for _, acteur := range r.Acteurs {
				acteur.Act()
			}

		}
	}
}

// EstOuvert retourne vrai si le restaurant est ouvert
func (r *Resto) EstOuvert() bool {
	for _, v := range r.Horaires {
		if float64(r.Temps) > v[0] && float64(r.Temps) < v[1] {
			return true
		}
	}
	return false
}

// Répartit répartit un espace de dimensions width, height en nb rectangles de façon
// à ce que le nombre de lignes et de colomnes soient le plus proche possible.
//
// Retourne un tableau de tableaux de coordonnées des rectangles sous la forme:
//	[
//		[XBasGauche, YBasGauche, XHautDroite, YHautDroite],
//		...
//	]
//
// 1 objet occupe tout l'espace,
// 2 objets: divisé en 2 verticalement,
//
// 12 objets: 4 colomnes de 3 objets
//
// 45 objets: 3 colomnes de 7 objets, 3 colomnes de 6 objets
func Répartit(width, height, nb int) [][4]int {
	if nb == 0 {
		return nil
	}
	w := int(math.Ceil(math.Sqrt(float64(nb))))
	h := int(math.Sqrt(float64(nb)))
	returned := make([][4]int, nb)
	index, shift := 0, 0
	switch {
	case nb < w*h:
		repLoop(width*(h-(w*h-nb))/h, height, h-(h*w-nb), w, &index, &shift, returned)
		repLoop(width*(w*h-nb)/h, height, h*w-nb, h, &index, &shift, returned)
	case nb == w*h:
		repLoop(width, height, w, h, &index, &shift, returned)
	case nb > w*h:
		repLoop(width*(nb-w*h)/w, height, nb-h*w, w, &index, &shift, returned)
		repLoop(width*(w-(nb-w*h))/w, height, w-(nb-h*w), h, &index, &shift, returned)
	}
	return returned
}
func repLoop(width, height, w, h int, index, shift *int, returned [][4]int) {
	for i := 0; i < w; i++ {
		he := 0
		for j := 0; j < h; j++ {
			returned[*index] = [4]int{*shift, he, *shift + width/w, he + height/h}
			he += height / h
			*index++
		}
		*shift += width / w
	}
}

// Carré est un ensemble de tables dont un groupe de serveurs s'occupe.
type Carré struct {
	// BasGaucheX, BasGaucheY, HautDroiteX, HautDroiteY
	Coords   [4]int
	Tables   []*Table
	Serveurs []*Serveur
	Resto    *Resto
}

func NewCarré(pos [4]int, carré map[string]interface{}, resto *Resto) *Carré {
	c := Carré{Coords: pos, Resto: resto}

	// Répartition de l'espace disponible entre les tables, placement des tables
	var tailles []int
	for k, v := range carré {
		taille, _ := strconv.Atoi(k)
		for i := 0.0; i < v.(float64); i++ {
			tailles = append(tailles, taille)
		}
	}
	for i := range tailles { // Mélange pour ne pas avoir les tables de même taille ensemble
		j := rand.Intn(i + 1)
		tailles[i], tailles[j] = tailles[j], tailles[i]
	}
	tablePos := Répartit(
		c.Coords[2]-c.Coords[0],
		c.Coords[3]-c.Coords[1],
		len(tailles),
	)
	// Création des tables
	for i := range tablePos {
		go func(i int) {
			table := NewTable(
				tailles[i],
				[4]int{
					pos[0] + tablePos[i][0], pos[1] + tablePos[i][1],
					pos[0] + tablePos[i][2], pos[1] + tablePos[i][3],
				},
				&c,
			)
			c.Tables = append(c.Tables, table)
		}(i)
	}
	for i := 0; i <= len(tailles)/8; i++ {
		c.Serveurs = append(c.Serveurs, NewServeur(&c))
	}
	return &c
}

// ServeurLibre retourne un serveur libre du carré,
// ou nil si aucun serveur n'est disponible.
func (c *Carré) ServeurLibre() *Serveur {
	for _, serveur := range c.Serveurs {
		if serveur.Etat == "Libre" {
			return serveur
		}
	}
	return nil
}

type Table struct {
	Sprite  *view.Sprite
	Contenu *view.Sprite // Couvert ou plat sur la table
	Carré   *Carré
	Taille  int
	Coords  [4]int
	Occupée bool
}

func NewTable(taille int, coords [4]int, c *Carré) *Table {
	table := Table{
		Taille: taille,
		Carré:  c,
		Sprite: c.Resto.Fenêtre.NewSprite(fmt.Sprintf("table%v.png", taille), 0.85),
	}
	table.Sprite.Pos(float64((coords[2]+coords[0])/2), float64(coords[3]+coords[1])/2)
	table.Sprite.Nom = "Une table"
	table.Sprite.Desc = table.String
	return &table
}

func (t *Table) String() string {
	if t.Occupée {
		return fmt.Sprintf("Table de %v personnes occupée", t.Taille)
	}
	return fmt.Sprintf("Table de %v personnes libre", t.Taille)
}

// Affiche le sprite de ce qui est sur la table.
func (t *Table) Afficher(quoi string, scale float64) {
	if t.Contenu == nil {
		t.Contenu = t.Carré.Resto.Fenêtre.NewSprite(quoi+".png", scale)
		t.Contenu.Desc = t.String
		t.Contenu.Pos(t.Sprite.Matrix[4], t.Sprite.Matrix[5])
	} else {
		t.Contenu.ChangeImg(quoi+".png", scale)
	}
}
