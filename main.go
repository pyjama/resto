// Partie "salle" du projet C#/.NET (sans C# ni .NET).
// Affiche et gère le restaurant, communique avec un serveur qui gère la cuisine.
//
// Tournez la molette pour accélérer/ralentir le temps et
// clickez sur un élément pour afficher des détails sur cet élément.
package main

import (
	"github.com/andlabs/ui"
	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/pyjama/resto/controller"
	"math/rand"
	"os"
	"time"
)

const (
	width   = 1280
	height  = 704
	adresse = "http://127.0.0.1:9090"
	//adresse = "http://192.168.43.63:8080"

	// Pour le serveur temporaire, uniquement utilisé en attendant de lier les deux projets
	acceleration = 60 // Accélération du temps
	port         = "9090"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	go func() {
		err := ui.Main(func() {})
		if err != nil {
			panic(err)
		}
	}()
	err := os.Chdir(os.Getenv("GOPATH") + "/src/gitlab.com/pyjama/resto")
	if err != nil {
		panic(err)
	}
	pixelgl.Run(run)
}

func run() {
	// Temporaire en attendant de lier les deux projets:
	go Serv(port, acceleration)
	time.Sleep(50 * time.Millisecond)

	game := controller.NewGame(width, height, adresse)
	fin := make(chan bool)
	for i, r := range game.Restos {
		go func(i int, r *controller.Resto) {
			// Supprime le restaurant quand sa fenêtre est fermée
			<-r.Fenêtre.Fin
			if len(game.Restos) > 1 {
				game.Restos[i] = game.Restos[len(game.Restos)-1]
				game.Restos = game.Restos[:len(game.Restos)-1]
			} else {
				fin <- true
			}
		}(i, r)
	}
	<-fin
}
