package view

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"image"
	"time"
)

// Fenêtre est la fenêtre possédée par chaque restaurant.
// Chaque fenêtre possède un tableau de sprites à afficher.
type Fenêtre struct {
	PxlWindow *pixelgl.Window
	Sprites   []*Sprite
	Title     string
	Fin       chan bool
	Scroll    chan float64
	Fps       int
}

func NewFenêtre(width, height int) *Fenêtre {
	w, err := pixelgl.NewWindow(pixelgl.WindowConfig{
		Title:  "La salle du resto",
		Bounds: pixel.R(0, 0, float64(width), float64(height)),
	})
	if err != nil {
		panic(err)
	}
	win := Fenêtre{
		PxlWindow: w,
		Fin:       make(chan bool),
		Scroll:    make(chan float64),
	}
	win.NewSprite("map.png", 2)
	go win.Draw()
	return &win
}

// Draw est la "boucle principale" de l'interface graphique.
// Gère les entrées de l'utilisateur et l'affichage.
func (f *Fenêtre) Draw() {
	sec := time.Tick(time.Second)
	frames := 0
	refresh := time.Tick(time.Second / time.Duration(60))
	for !f.PxlWindow.Closed() {
		if f.PxlWindow.JustPressed(pixelgl.MouseButtonLeft) {
			f.CheckClick(f.PxlWindow.MousePosition())
		}
		if f.PxlWindow.MouseScroll().Y != 0 {
			f.Scroll <- f.PxlWindow.MouseScroll().Y
		}
		f.PxlWindow.Clear(image.Black)
		for i := range f.Sprites {
			if f.Sprites[i] == nil {
				f.Sprites = append(f.Sprites[:i], f.Sprites[i+1:]...)
			}
			f.Sprites[i].PxlSprite.Draw(f.PxlWindow, f.Sprites[i].Matrix)
		}
		f.PxlWindow.SetTitle(f.Title)
		select {
		case <-sec:
			f.Fps, frames = frames, 0
		case <-refresh:
			frames++
		}
		f.PxlWindow.Update()
	}
	f.Fin <- true
	f.PxlWindow.Destroy()
}

// CheckClick vérifie si un sprite est sous le curseur
// et ouvre un popup décrivant l'élément clické si c'est le cas
func (f *Fenêtre) CheckClick(mousePos pixel.Vec) {
	// Démarre à 1 car f.Sprites[0] est l'image de fond
	for i := len(f.Sprites) - 1; i > 0; i-- {
		mp := mousePos
		rec := f.Sprites[i].PxlSprite.Frame()
		mat := f.Sprites[i].Matrix
		mp.X += (rec.Max.X/2)*mat[0] + rec.Min.X/2
		mp.Y += (rec.Max.Y/2)*mat[3] + rec.Min.Y/2
		if (rec.Min.X+mat[4] < mp.X) && (rec.Max.X*mat[0]+mat[4] > mp.X) {
			if (rec.Min.Y+mat[5] < mp.Y) && (rec.Max.Y*mat[3]+mat[5] > mp.Y) {
				f.Sprites[i].Popup()
				return
			}
		}
	}
}

// Supprime le sprite de la vue
func (f *Fenêtre) Delete(sprite *Sprite) {
	for i := range f.Sprites {
		if f.Sprites[i] == sprite {
			f.Sprites = append(f.Sprites[:i], f.Sprites[:i+1]...)
		}
	}
}
