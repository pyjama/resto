package view

import (
	"github.com/andlabs/ui"
	"github.com/faiface/pixel"
	"image"
	_ "image/png"
	"math/rand"
	"os"
)

// Sprite contient le sprite à afficher et sa matrice.
// Toutes les entités s'affichant à l'écran doivent avoir un attribut *Sprite.
// Le constructeur place un pointeur vers l'objet créé dans la fenêtre.
type Sprite struct {
	PxlSprite *pixel.Sprite
	Matrix    pixel.Matrix
	Nom       string
	Desc      func() string
}

// NewSprite ajoute un sprite à l'interface graphique
func (f *Fenêtre) NewSprite(path string, scale float64) *Sprite {
	img, err := LoadPicture(path)
	if err != nil {
		panic(err)
	}
	sprite := Sprite{
		PxlSprite: pixel.NewSprite(img, img.Bounds()),
		Matrix:    pixel.IM.Moved(f.PxlWindow.Bounds().Center()),
	}
	sprite.Matrix = sprite.Matrix.Scaled(f.PxlWindow.Bounds().Center(), scale)
	f.Sprites = append(f.Sprites, &sprite)
	return &sprite
}

// NewRandomSPrite ajoute un sprite à l'interface graphique
// à partir d'une image en contenant plusieurs
// path chemin de l'image, x, y dimensions d'un sprite, scale échelle utilisée.
func (f *Fenêtre) NewRandomSprite(path string, x, y int, scale float64) *Sprite {
	spritesheet, err := LoadPicture(path)
	if err != nil {
		panic(err)
	}
	// Position du sprite séléctionné dans le spritesheet
	bounds := spritesheet.Bounds()
	posX := float64(rand.Intn(int(bounds.Max.X/float64(x))) * x)
	posY := float64(rand.Intn(int(bounds.Max.Y/float64(y))) * y)
	var sprite Sprite
	sprite.PxlSprite = pixel.NewSprite(
		spritesheet,
		pixel.R(posX, posY, posX+float64(x), posY+float64(y)),
	)
	sprite.Matrix = pixel.IM.Moved(f.PxlWindow.Bounds().Center())
	sprite.Matrix = sprite.Matrix.Scaled(f.PxlWindow.Bounds().Center(), scale)
	f.Sprites = append(f.Sprites, &sprite)
	return &sprite
}

func (s *Sprite) ChangeImg(path string, scale float64) {
	img, err := LoadPicture(path)
	if err != nil {
		panic(err)
	}
	s.PxlSprite = pixel.NewSprite(img, img.Bounds())
	s.Matrix[0], s.Matrix[3] = scale, scale
}

// Moveto déplace un sprite vers le centre du sprite dest + x pixels horizontalement et y pixels verticalement.
// Retourne "true" si le sprite est arrivé à destination
func (s *Sprite) Moveto(dest *Sprite, x, y float64) bool {
	ox, oy := s.Matrix[4], s.Matrix[5]
	if dest.Matrix[4]+x > s.Matrix[4] {
		s.Move(4, 0)
	}
	if dest.Matrix[4]+x < s.Matrix[4] {
		s.Move(-4, 0)
	}
	if dest.Matrix[5]+y > s.Matrix[5] {
		s.Move(0, 4)
	}
	if dest.Matrix[5]+y < s.Matrix[5] {
		s.Move(0, -4)
	}
	if ox == s.Matrix[4] && oy == s.Matrix[5] {
		return true
	}
	return false
}

// Move déplace le sprite de x pixels horizontalement et y verticalement
func (s *Sprite) Move(x, y float64) {
	s.Matrix = s.Matrix.Moved(pixel.V(x, y))
}

// Pos place immédiatement le sprite à la position x, y
func (s *Sprite) Pos(x, y float64) {
	s.Matrix[4] = x
	s.Matrix[5] = y
}

func (s *Sprite) Popup() {
	ui.QueueMain(func() {
		win := ui.NewWindow(s.Nom, 300, 200, false)
		win.SetMargined(false)
		win.SetBorderless(true)
		box := ui.NewVerticalBox()
		box.Append(ui.NewLabel(s.Nom+"\n\n"+s.Desc()), false)
		button := ui.NewButton("Fermer")
		box.Append(button, false)
		win.SetChild(box)
		button.OnClicked(func(*ui.Button) { win.Destroy() })
		win.Show()
	})
}

// LoadPicture charge une image pour pixel
func LoadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open("ressources/" + path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}
